import Vue from "vue";
import VueRouter from "vue-router";
// import Home from '../views/Home.vue'
import Welcome from "../views/Welcome.vue";
// import OCFormatter from '../views/OCFormatter.vue'

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "Welcome",
    component: Welcome,
  },
  {
    path: "/OCFormatter",
    name: "OCFormatter",
    // component: OCFormatter

    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/OCFormatter.vue"),
  },
  {
    path: "/generateDataMock",
    name: "GenerateDataMock",
    // component: OCFormatter

    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/GenerateDataMock.vue"),
  },
  {
    path: "/moduleGenerator",
    name: "ModuleGenerator",
    // component: OCFormatter

    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(/* webpackChunkName: "about" */ "../views/ModuleGenerator.vue"),
  },
  {
    path: "/descriptorValidator",
    name: "DescriptorValidator",
    // component: OCFormatter

    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () =>
      import(
        /* webpackChunkName: "about" */ "../views/DescriptorValidator.vue"
      ),
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;
